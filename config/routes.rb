Rails.application.routes.draw do
  devise_for :users


  authenticated :user do
    root :to => 'main#home'
  end
  unauthenticated :user do
    devise_scope :user do
      root :to => "main#unregistered", as: :unregistered_root
    end
  end

  get "/demo",to: "main#demo"

	
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
