module ApplicationHelper
	def center_div args = {}, &block
		css_classes = (args.has_key?(:class)) ? args[:class] : ""
		html = content_tag :div, class:"row center-xs #{css_classes}" do
			grid md:11, lg:11,xs:12 do
				capture(&block)
			end
		end
		html.html_safe
	end
end
